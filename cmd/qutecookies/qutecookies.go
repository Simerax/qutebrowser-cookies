package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"os"

	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/pflag"
)

type Cookie struct {
	CreationUTC    int    `json:"creation_utc"`
	HostKey        string `json:"host_key"`
	Name           string `json:"name"`
	Value          string `json:"value"`
	Path           string `json:"path"`
	ExpiresUTC     int    `json:"expires_utc"`
	IsSecure       int    `json:"is_secure"`
	IsHTTPOnly     int    `json:"is_httponly"`
	LastAccessUTC  int    `json:"last_access_utc"`
	HasExpires     int    `json:"has_expires"`
	IsPersistent   int    `json:"is_persistent"`
	Priority       int    `json:"priority"`
	EncryptedValue []byte `json:"encrypted_value"`
	SameSite       int    `json:"samesite"`
	SourceScheme   int    `json:"source_scheme"`
}

const (
	DefaultCookiesPath = "$HOME/.local/share/qutebrowser/webengine/Cookies"
)

func main() {

	databasePath := pflag.StringP("path", "p", DefaultCookiesPath, "Path to the Cookies sqlite3 Database")
	deleteAllCookies := pflag.Bool("delete-all-cookies", false, "Deletes every Cookie")
	exportJSON := pflag.Bool("json", false, "Dump the Database as JSON")
	importJSONFile := pflag.String("import-json", "", "Fills the database with the given Cookies. Updates existing Cookies and creates new ones. Does not delete cookies")

	pflag.Usage = func() {
		pflag.PrintDefaults()
		usage := `
		
  Usage Example:

  // Export current cookies
  qutecookies --json > cookies.json
  
  // Edit the cookies in whatever way you want 
  nvim cookies.json
  
  // Delete the existing cookies and import the changed cookies 
  qutecookies --delete-all-cookies --import-json cookies.json

  // A little more complicated: delete all cookies except the ones from 'duckduckgo'
  qutecookies --json | jq '[ .[] | select(.host_key | contains("duckduckgo")) ]' | qutecookies --delete-all-cookies`

		fmt.Println(usage)
		os.Exit(0)
	}
	pflag.Parse()

	*databasePath = os.ExpandEnv(*databasePath)

	db, err := sql.Open("sqlite3", *databasePath)
	ExitOnError(err)
	defer db.Close()

	if *exportJSON {
		dumpJSON(db)
	} else if *importJSONFile != "" {

		if *deleteAllCookies {
			_, err := db.Exec("DELETE FROM cookies;")
			ExitOnError(err)
		}

		file, err := os.Open(*importJSONFile)
		ExitOnError(err)
		defer file.Close()
		importJSON(db, file)
	} else if *deleteAllCookies {
		_, err := db.Exec("DELETE FROM cookies;")
		ExitOnError(err)

		if inputFromPipe() {
			importJSON(db, os.Stdin)
		}
	} else {

		if inputFromPipe() {
			importJSON(db, os.Stdin)
		} else {
			pflag.Usage()
		}
	}
}

func inputFromPipe() bool {
	fileInfo, err := os.Stdin.Stat()
	ExitOnError(err)
	return fileInfo.Mode()&os.ModeCharDevice == 0
}

func ExitOnError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func IterateCompleteDatabase(db *sql.DB, onRow func(c Cookie)) {
	row, err := db.Query("select creation_utc, host_key, name, value, path, expires_utc, is_secure, is_httponly, last_access_utc, has_expires, is_persistent, priority, encrypted_value, samesite, source_scheme from cookies;")
	ExitOnError(err)
	defer row.Close()
	cookie := Cookie{}
	for row.Next() {
		row.Scan(
			&cookie.CreationUTC,
			&cookie.HostKey,
			&cookie.Name,
			&cookie.Value,
			&cookie.Path,
			&cookie.ExpiresUTC,
			&cookie.IsSecure,
			&cookie.IsHTTPOnly,
			&cookie.LastAccessUTC,
			&cookie.HasExpires,
			&cookie.IsPersistent,
			&cookie.Priority,
			&cookie.EncryptedValue,
			&cookie.SameSite,
			&cookie.SourceScheme,
		)
		onRow(cookie)
	}
}

func dumpJSON(db *sql.DB) {

	cookies := make([]Cookie, 0)
	IterateCompleteDatabase(db, func(c Cookie) {
		cookies = append(cookies, c)
	})
	json, err := json.Marshal(cookies)
	ExitOnError(err)
	fmt.Print(string(json))
}

func importJSON(db *sql.DB, in io.Reader) {

	decoder := json.NewDecoder(in)
	cookies := make([]Cookie, 0)

	err := decoder.Decode(&cookies)
	ExitOnError(err)

	tx, err := db.Begin()
	ExitOnError(err)

	stmt, err := tx.Prepare("select * from cookies where host_key=? and name=? and path=?")

	ExitOnError(err)
	updateStmt, err := tx.Prepare("update cookies set creation_utc=?, host_key=?, name=?, value=?, path=?, expires_utc=?, is_secure=?, is_httponly=?, last_access_utc=?, has_expires=?, is_persistent=?, priority=?, encrypted_value=?, samesite=?, source_scheme=? where host_key=? and name=? and path=?")
	ExitOnError(err)
	insertStmt, err := tx.Prepare("insert into cookies (creation_utc, host_key, name, value, path, expires_utc, is_secure, is_httponly, last_access_utc, has_expires, is_persistent, priority, encrypted_value, samesite, source_scheme) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
	ExitOnError(err)
	for _, cookie := range cookies {
		rows, err := stmt.Query(cookie.HostKey, cookie.Name, cookie.Path)
		ExitOnError(err)

		if rows.Next() {
			ExitOnError(err)
			_, err = updateStmt.Exec(
				cookie.CreationUTC,
				cookie.HostKey,
				cookie.Name,
				cookie.Value,
				cookie.Path,
				cookie.ExpiresUTC,
				cookie.IsSecure,
				cookie.IsHTTPOnly,
				cookie.LastAccessUTC,
				cookie.HasExpires,
				cookie.IsPersistent,
				cookie.Priority,
				cookie.EncryptedValue,
				cookie.SameSite,
				cookie.SourceScheme,

				// Update Constraint
				cookie.HostKey,
				cookie.Name,
				cookie.Path,
			)
			ExitOnError(err)
		} else {
			_, err = insertStmt.Exec(
				cookie.CreationUTC,
				cookie.HostKey,
				cookie.Name,
				cookie.Value,
				cookie.Path,
				cookie.ExpiresUTC,
				cookie.IsSecure,
				cookie.IsHTTPOnly,
				cookie.LastAccessUTC,
				cookie.HasExpires,
				cookie.IsPersistent,
				cookie.Priority,
				cookie.EncryptedValue,
				cookie.SameSite,
				cookie.SourceScheme,
			)
		}
		rows.Close()
	}

	err = tx.Commit()
	ExitOnError(err)
}

/*

creation_utc INTEGER NOT NULL,
host_key TEXT NOT NULL,
name TEXT NOT NULL,
value TEXT NOT NULL,
path TEXT NOT NULL,
expires_utc INTEGER NOT NULL,
is_secure INTEGER NOT NULL,
is_httponly INTEGER NOT NULL,
last_access_utc INTEGER NOT NULL,
has_expires INTEGER NOT NULL DEFAULT 1,
is_persistent INTEGER NOT NULL DEFAULT 1,
priority INTEGER NOT NULL DEFAULT 1,
encrypted_value BLOB DEFAULT '',
samesite INTEGER NOT NULL DEFAULT -1,
source_scheme INTEGER NOT NULL DEFAULT 0,
UNIQUE (host_key, name, path));

*/
