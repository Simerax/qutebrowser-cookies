# qutebrowser-cookies

deleting, exporting and importing of cookies for qutebrowser


```
      --delete-all-cookies   Deletes every Cookie
      --import-json string   Fills the database with the given Cookies. Updates existing Cookies and creates new ones. Does not delete cookies
      --json                 Dump the Database as JSON
  -p, --path string          Path to the Cookies sqlite3 Database (default "$HOME/.local/share/qutebrowser/webengine/Cookies")

		
  Usage Example:

  // Export current cookies
  qutecookies --json > cookies.json

  // Edit the cookies in whatever way you want
  nvim cookies.json

  // Delete the existing cookies and import the changed cookies
  qutecookies --delete-all-cookies --import-json cookies.json

  // A little more complicated: delete all cookies except the ones from 'duckduckgo'
  qutecookies --json | jq '[ .[] | select(.host_key | contains("duckduckgo")) ]' | qutecookies --delete-all-cookies
```
