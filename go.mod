module gitlab.com/Simerax/qutebrowser-cookies

go 1.15

require (
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/spf13/pflag v1.0.5
)
